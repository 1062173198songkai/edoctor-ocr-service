package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class EdoctorApplication {


    public static void main(String[] args) {
        System.out.println("SpringBoot Start1....");
        try {
            SpringApplication.run(EdoctorApplication.class, args);

        }catch (Throwable throwable){
            throwable.printStackTrace();
        }
        System.out.println("SpringBoot Start2....");

    }
}
