package com.example.demo.search.grpc.config;

import com.example.demo.edoctor.grpc.OcrGrpcClient;
import com.example.demo.search.grpc.QueryCorrectGrpcClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SearchGrpcClientConfig {
    private static final String HOST="localhost";
    private static final int QUERY_CORRECT_PORT = 19022;
    
    @Bean
    public QueryCorrectGrpcClient queryCorrectGrpcClient(){
        QueryCorrectGrpcClient queryCorrectGrpcClient  =new QueryCorrectGrpcClient(HOST,QUERY_CORRECT_PORT);
        return queryCorrectGrpcClient;
    }
}
