package com.example.demo.search.grpc;

import com.example.demo.edoctor.grpc.QueryCorrectGrpc;
import com.example.demo.edoctor.grpc.QueryReply;
import com.example.demo.edoctor.grpc.QueryRequest;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.concurrent.TimeUnit;

public class QueryCorrectGrpcClient {
    private final ManagedChannel channel;
    private final QueryCorrectGrpc.QueryCorrectBlockingStub blockingStub;
    private final QueryCorrectGrpc.QueryCorrectStub queryCorrectStub;

    public QueryCorrectGrpcClient(String host,int port){
        this(ManagedChannelBuilder.forAddress(host,port).usePlaintext().build());
    }
    QueryCorrectGrpcClient(ManagedChannel channel){
        this.channel = channel;
        this.blockingStub = QueryCorrectGrpc.newBlockingStub(channel);
        this.queryCorrectStub = QueryCorrectGrpc.newStub(channel);
    }
    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }
    /**
     * send to server.
     */
    public String queryCorrect(String query){
        QueryRequest queryRequest = QueryRequest.newBuilder().setQuery(query).build();
        QueryReply queryReply;
        try {
            queryReply = blockingStub.sendQuery(queryRequest);
            return queryReply.getResult();
        }catch (Throwable throwable){
            throwable.printStackTrace();
            return  null;
        }
    }
    public static void main(String[] arg0){
        String correctQuery  = new QueryCorrectGrpcClient("localhost",19022).queryCorrect("李子染");
        System.out.println(correctQuery);
    }
}
