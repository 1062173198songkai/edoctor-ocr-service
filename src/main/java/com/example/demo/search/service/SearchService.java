package com.example.demo.search.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.example.demo.eventtracing.entity.HotEntity;
import com.example.demo.eventtracing.entity.SugEntity;
import com.example.demo.eventtracing.hbase.HbaseClient;
import com.example.demo.search.entity.EdoctorQuestionAnswer;
import com.example.demo.search.entity.HotWord;
import com.example.demo.search.entity.PageObject;
import com.example.demo.search.entity.Question;
import com.example.demo.search.grpc.QueryCorrectGrpcClient;
import com.example.demo.search.repository.EdoctorMysqlDataExport;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.apache.ibatis.reflection.wrapper.BaseWrapper;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
public class SearchService implements ApplicationRunner {

    private HbaseClient hbaseClient;
    private RestHighLevelClient elasticsearchClient;
    private EdoctorMysqlDataExport edoctorMysqlDataExport;
    private QueryCorrectGrpcClient queryCorrectGrpcClient;
    private Random random = new Random();
    private ExecutorService threadPool = Executors.newCachedThreadPool();


    public SearchService(RestHighLevelClient elasticsearchClient,HbaseClient hbaseClient,EdoctorMysqlDataExport edoctorMysqlDataExport,QueryCorrectGrpcClient queryCorrectGrpcClient){

        this.elasticsearchClient = elasticsearchClient;
        this.hbaseClient = hbaseClient;
        this.edoctorMysqlDataExport = edoctorMysqlDataExport;
        this.queryCorrectGrpcClient = queryCorrectGrpcClient;
    }
    private static final String RED = "red";
    private static final String WHITE = "white";
    private static final String HOT_ICON_TEXT = "热";
    private static final int RATE = 4;
    private static final int MAX_HOT_VALUES = 100000;
    public static final String SUG_SEARCH_DOC = "questionsug/_search";
    public static final String SEARCH_DOC ="questions/_search";
    public static final String DESP_TAG="...";
    public static final String DEFAULT_SIZE = "5";
//    public static final String DEFAULT_SIZE = "3";

    public static final String PUT_QUESTION = "{" +
            "  \"question\":\"%s\"," +
            "  \"answer\":\"%s\"" +
            "}";
    public static final String ANALYSIS_IK = "{" +
            "  \"analyzer\": \"ik_smart\"," +
            "  \"text\": [\"%s\"]" +
            "}";
    public static final String PUT_SUG_NAME = "{" +
            "  \"name\":\"%s\"" +
            "}";
    public static final String NOT_FIRST_QUERY = "{" +
            "  \"size\": %s, " +
            "  \"sort\": [{\"_score\":\"desc\"},{\"_id\":\"desc\"}]," +
            "  \"search_after\":[%s,\"%s\"]," +
            "  \"query\": {" +
            "    \"multi_match\": {" +
            "      \"query\": \"%s\"," +
            "      \"fields\": [\"question\",\"answer\"]" +
            "    }" +
            "  }," +
            "  \"highlight\": {" +
            "    \"pre_tags\": \"<div style=\\\"color:red;display:inline\\\">\", " +
            "    \"post_tags\": \"</div>\", " +
            "    \"fields\": {" +
            "      \"question\":{}," +
            "      \"answer\":{}" +
            "    }" +
            "  }" +
            "}";
    public static final String FIRST_QUERY = "{" +
            "  \"size\": %s, " +
            "  \"sort\": [{\"_score\":\"desc\"},{\"_id\":\"desc\"}]," +
            "  \"query\": {" +
            "    \"multi_match\": {" +
            "      \"query\": \"%s\"," +
            "      \"fields\": [\"question\",\"answer\"]" +
            "    }" +
            "  }," +
            "  \"highlight\": {" +
            "    \"pre_tags\": \"<div style=\\\"color:red;display:inline\\\">\", " +
            "    \"post_tags\": \"</div>\", " +
            "    \"fields\": {" +
            "      \"question\":{}," +
            "      \"answer\":{}" +
            "    }" +
            "  }" +
            "}";
    public static final String SUG_SEARCH_STR="{" +
            "  \"_source\": false," +
            "  \"suggest\": {" +
            "    \"star_name_suggestion\": {" +
            "      \"prefix\":\"%s\"," +
            "      \"completion\":{" +
            "        \"field\":\"name\"," +
            "        \"size\":10," +
            "        \"skip_duplicates\":true" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}";

    public String queryCorrect(String query){
//        QueryCorrectCallable queryCorrectCallable = new QueryCorrectCallable(queryCorrectGrpcClient,query);
//        Future<String> queryCorrectFuture = threadPool.submit(queryCorrectCallable);
        return queryCorrectGrpcClient.queryCorrect(query);
    }
    public List<HotWord> getHotWords(){
        List<HotEntity> hotEntityStrList = hbaseClient.getHotWords();
        //900-1000之间
        int maxHot = (random.nextInt(100) + MAX_HOT_VALUES -100)/hotEntityStrList.get(0).getNum();
        ArrayList<HotWord> hotWordStrList = new ArrayList<>(hotEntityStrList.size());
        for(HotEntity hotEntity: hotEntityStrList){
            String iconContent;
            String iconColor;
            if(isHot()){
                iconColor = RED;
                iconContent = HOT_ICON_TEXT;
            }else {
                iconColor = WHITE;
                iconContent = "";
            }
            hotWordStrList.add(new HotWord(hotEntity.getHotword(),String.valueOf(hotEntity.getNum()*maxHot),new HotWord.Icon(iconContent,iconColor)));
        }
        return hotWordStrList;
    }
    private boolean isHot(){
        int num = random.nextInt(RATE);
        if(num == 0){
            return true;
        }
        return false;
    }
    public List<SugEntity> suggest(String query){
        //查询正常结果
        Request request = new Request("GET", SUG_SEARCH_DOC);
        request.setJsonEntity(String.format(SUG_SEARCH_STR,query));
        Response response = null;
        try {
            response = elasticsearchClient.getLowLevelClient().performRequest(request);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        HttpEntity httpEntity = response.getEntity();
        String str;
        try {
            str = EntityUtils.toString(httpEntity);
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
        JSONObject jsonObject = JSONObject.parseObject(str);
        JSONArray jsonArray;
        try {
            jsonArray = jsonObject.getJSONObject("suggest").getJSONArray("star_name_suggestion").getJSONObject(0).getJSONArray("options");
            ArrayList<SugEntity> suggestionList = new ArrayList<SugEntity>(jsonArray.size());
            for(int i = 0 ; i < jsonArray.size() ; i++){
                try {
                    suggestionList.add(new SugEntity(jsonArray.getJSONObject(i).getString("text")));
//                    suggestionList.add(jsonArray.getJSONObject(i).getString("text"));
                }catch (Throwable throwable){
                    throwable.printStackTrace();
                }
            }
            return suggestionList;
        }catch (Throwable throwable){
            throwable.printStackTrace();
            return null;
        }
    }
    public PageObject queryBypage(String query, String rate, String id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                hbaseClient.insertHistoryData(query);
            }
        }).start();
        //非首次查询
        if (rate != null && id != null) {
            return queryByPageNotFirst(query,rate,id);
        }
        //查询正常结果
        Request request = new Request("GET", SEARCH_DOC);
        System.out.println(String.format(FIRST_QUERY, DEFAULT_SIZE,query));
        request.setJsonEntity(String.format(FIRST_QUERY, DEFAULT_SIZE,query));
        return getPageObject(request);
    }
    //传入hits的JSONARRAY
    private JSONArray getSortInfo(JSONArray jsonArray){
        try {
            return jsonArray.getJSONObject(jsonArray.size()-1).getJSONArray("sort");
        }catch (Throwable throwable){
            return null;
        }
    }

    /**
     * 非首次搜索求请求 即自然结果翻页请求
     * @param query
     *
     * @param rate
     * @param id
     * @return
     */
    private PageObject queryByPageNotFirst(String query,String rate,String id){
        //查询正常结果
        Request request = new Request("GET", "questions/_search");

        request.setJsonEntity(String.format(NOT_FIRST_QUERY, DEFAULT_SIZE,rate,id,query));
        new Thread(new Runnable() {
            @Override
            public void run() {
                hbaseClient.insertHistoryData(query);
            }
        }).start();
        return getPageObject(request);
    }
    //搜索结果是否为空
    private boolean isEmptyResult (JSONObject jsonObject){

        if(jsonObject.getJSONObject("hits").getJSONArray("hits").size() == 0){
            return true;
        }else {
            return false;
        }
    }
    private PageObject getPageObject(Request request) {
        Response response = null;
        try {
            response = elasticsearchClient.getLowLevelClient().performRequest(request);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        HttpEntity httpEntity = response.getEntity();
        String str;
        try {
            str = EntityUtils.toString(httpEntity);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        System.out.println("response:"+response);
        JSONObject jsonObject = JSONObject.parseObject(str);
        //搜索结果为空
        if(isEmptyResult(jsonObject)){
            return new PageObject(new ArrayList<>(0),null);
        }
        JSONArray jsonArray = jsonObject.getJSONObject("hits").getJSONArray("hits");
        LinkedHashMap<String,Question> questionLinkedHashMap = new LinkedHashMap<>();
        ArrayList<Question> questions = new ArrayList<Question>(jsonArray.size());
        for(int i = 0 ; i < jsonArray.size() ; i ++){
            StringBuilder questionHighStringBuilder = new StringBuilder();
            StringBuilder answerHighStringBuilder = new StringBuilder();
            //处理highlight数组
            try {
                JSONObject jsonObjectHighLight = jsonArray.getJSONObject(i).getJSONObject("highlight");
                for (int j = 0 ; j < jsonObjectHighLight.getJSONArray("question").size() ; j ++){
                    questionHighStringBuilder.append(jsonObjectHighLight.getJSONArray("question").getString(j)).append(DESP_TAG);
                }
            }catch (Throwable throwable){
                throwable.printStackTrace();
            }
            try {
                JSONObject jsonObjectHighLight = jsonArray.getJSONObject(i).getJSONObject("highlight");
                for (int j = 0 ; j < jsonObjectHighLight.getJSONArray("answer").size() ; j ++){
                    answerHighStringBuilder.append(jsonObjectHighLight.getJSONArray("answer").getString(j)).append(DESP_TAG);
                }
            }catch (Throwable throwable){
                throwable.printStackTrace();
            }
            try {
                Question temQuestion =new Question(jsonArray.getJSONObject(i).getString("_id"),jsonArray.getJSONObject(i).getJSONObject("_source").getString("question"),jsonArray.getJSONObject(i).getJSONObject("_source").getString("answer"),
                        questionHighStringBuilder.toString(),answerHighStringBuilder.toString());
                questionLinkedHashMap.put(temQuestion.getAnswer()+temQuestion.getQuestion(),temQuestion);
            }catch (Throwable throwable){
                throwable.printStackTrace();
            }
        }
        for(Map.Entry<String,Question> entry: questionLinkedHashMap.entrySet()){
            questions.add(entry.getValue());
        }
        return new PageObject(questions,getSortInfo(jsonArray));
    }
    private void exportMysqlDataToElasticSearch(){
        System.out.println("");
        List<EdoctorQuestionAnswer> edoctorQuestionAnswerList =
                edoctorMysqlDataExport.selectList(null);
        LinkedHashMap<String,EdoctorQuestionAnswer> hashMap = new LinkedHashMap<>();
        for(EdoctorQuestionAnswer edoctorQuestionAnswer : edoctorQuestionAnswerList){
            hashMap.put(edoctorQuestionAnswer.getAnswer()+edoctorQuestionAnswer.getQuestion(),edoctorQuestionAnswer);
        }

        for(Map.Entry<String,EdoctorQuestionAnswer> entry:hashMap.entrySet()){
            EdoctorQuestionAnswer edoctorQuestionAnswer = entry.getValue();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    processEdoctorQuestionAnswer(edoctorQuestionAnswer);
                }
            }).start();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    AnalysisAndAddToQuestions(edoctorQuestionAnswer);
                }
            }).start();
        }
    }
    private void AnalysisAndAddToQuestions(EdoctorQuestionAnswer edoctorQuestionAnswer){
        Request requestAnalysis = new Request("GET","_analyze");
        requestAnalysis.setJsonEntity(String.format(ANALYSIS_IK,edoctorQuestionAnswer.getQuestion().trim().replaceAll("\r|\n|\\s","")));
        Response response = null;
        String str;
        try {
            response = elasticsearchClient.getLowLevelClient().performRequest(requestAnalysis);
            HttpEntity httpEntity = response.getEntity();
            str = EntityUtils.toString(httpEntity);
            List<String> words = processAnalysisResponse(JSONObject.parseObject(str));
            for(String word : words){
                Request request = new Request("POST","questionsug/_doc");
                request.setJsonEntity(String.format(PUT_SUG_NAME,word));
                elasticsearchClient.getLowLevelClient().performRequest(request);
            }
        }catch (Throwable throwable){
            throwable.printStackTrace();
            System.out.println(String.format(ANALYSIS_IK,edoctorQuestionAnswer.getQuestion()));
        }
    }
    private List<String> processAnalysisResponse(JSONObject jsonObject){
        ArrayList<String> words = new ArrayList<>();
        try {
            JSONArray jsonArray = jsonObject.getJSONArray("tokens");
            for(int i = 0 ; i <jsonArray.size() ; i ++){
                try {
                    words.add(jsonArray.getJSONObject(i).getString("token"));
                }catch (Throwable throwable){
                    throwable.printStackTrace();
                }
            }
        }catch (Throwable throwable){
            throwable.printStackTrace();
        }
        return words;
    }
    private void processEdoctorQuestionAnswer(EdoctorQuestionAnswer edoctorQuestionAnswer){
        Request requestQuestion = new Request("POST", "questions/_doc");
        requestQuestion.setJsonEntity(String.format(PUT_QUESTION,edoctorQuestionAnswer.getQuestion().replaceAll("\n", ""),edoctorQuestionAnswer.getAnswer().replaceAll("\n", "")));
        Response response = null;
        try {
            response = elasticsearchClient.getLowLevelClient().performRequest(requestQuestion);
        }catch (Throwable throwable){
            throwable.printStackTrace();
            System.out.println(String.format(PUT_QUESTION,edoctorQuestionAnswer.getQuestion().replaceAll("\n",""),edoctorQuestionAnswer.getAnswer().replaceAll("\n", "")));
        }
    }
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("searchservice初始化");
//        exportMysqlDataToElasticSearch();
//        System.out.println("mysql插入完成");

    }
}
