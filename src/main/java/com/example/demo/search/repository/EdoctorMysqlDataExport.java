package com.example.demo.search.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.search.entity.EdoctorQuestionAnswer;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EdoctorMysqlDataExport  extends BaseMapper<EdoctorQuestionAnswer> {
}
