package com.example.demo.search.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
public class HotWord {
    String content ;
    String hotvalue;
    Icon icon;
    @AllArgsConstructor
    @Data
    public static class Icon{
        String text;
        String backgroundcolor;
    }
}
