package com.example.demo.search.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Timestamp;

@Data
@TableName(value = "edoctor_question_answer")
public class EdoctorQuestionAnswer {

    private String answer;
    private String question;
    private Timestamp timestamp;
}
