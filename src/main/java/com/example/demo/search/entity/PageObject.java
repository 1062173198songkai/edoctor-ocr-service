package com.example.demo.search.entity;

import com.alibaba.fastjson.JSONArray;

import java.util.List;

public class PageObject {
    //搜索结果
    private List<Question> questions;
    //翻页信息
    private JSONArray sortJsonArray;

    public PageObject(List<Question> questions,JSONArray sortJsonArray){
        this.questions = questions;
        this.sortJsonArray = sortJsonArray;
    }
    public JSONArray getSortJsonArray() {
        return sortJsonArray;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public void setSortJsonArray(JSONArray sortJsonArray) {
        this.sortJsonArray = sortJsonArray;
    }
}
