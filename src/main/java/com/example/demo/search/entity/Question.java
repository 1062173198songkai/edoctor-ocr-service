package com.example.demo.search.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


@Data
@NoArgsConstructor
@Document(indexName = "question",type = "_doc")
public class Question {
    @Id
    String id;
    @Field(type = FieldType.Text)
    String question;
    @Field(type = FieldType.Text)
    String answer;
    String highLigtQuestion;
    String highLightAnswer;
    public Question(String id,String question,String answer,String highLigtQuestion,String highLightAnswer){
        this.id = id;
        this.question = question;
        this.answer = answer;
        this.highLightAnswer = highLightAnswer;
        this.highLigtQuestion = highLigtQuestion;
    }

}
