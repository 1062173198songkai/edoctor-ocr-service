package com.example.demo.search.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.search.service.SearchService;
import com.example.demo.search.entity.PageObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@RestController
public class SearchController implements ApplicationListener {
    @Autowired
    SearchService searchService;
    private ExecutorService threadPool = Executors.newCachedThreadPool();
    @CrossOrigin
    @GetMapping("/search/new")
    public String searchNew(@RequestParam(value = "query", required = true) String query, @RequestParam(value = "rate", required = false) String rate, @RequestParam(value = "id", required = false) String id) {
        Future<String> queryCorrectFuture = null;
        //首次查询
        if (rate == null && id == null) {
            QueryCorrectCallable queryCorrectCallable = new QueryCorrectCallable(query,searchService);
            queryCorrectFuture = threadPool.submit(queryCorrectCallable);
        }
        PageObject pageObject = searchService.queryBypage(query, rate, id);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result", pageObject);
        //
        try {
            if(queryCorrectFuture != null){
                while (!queryCorrectFuture.isDone()){
                }
                String correctQuery = queryCorrectFuture.get();
                if(!correctQuery.equals(query)){
                    jsonObject.put("isCorrect",true);
                    jsonObject.put("correcrtQuery",correctQuery);
                }else {
                    jsonObject.put("isCorrect",false);
                }
            }
        }catch (Throwable throwable){
            throwable.printStackTrace();
        }
        return jsonObject.toString();
    }
    @CrossOrigin
    @GetMapping("search/sug")
    public String searchSuggest(@RequestParam(value = "query", required = true) String query) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sug", searchService.suggest(query));
        return jsonObject.toString();
    }
    @CrossOrigin
    @GetMapping("/search/hot")
    public String searchHotWords() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("hot", searchService.getHotWords());
        return jsonObject.toString();
    }

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        System.out.println("searchcontroller 初始化");
    }
    @AllArgsConstructor
    @Data
    public static class  QueryCorrectCallable implements Callable<String> {
        String query;
        SearchService searchService;
        @Override
        public String call() throws Exception {
            return searchService.queryCorrect(query);
        }
    }
}
