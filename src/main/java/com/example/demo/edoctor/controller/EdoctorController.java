package com.example.demo.edoctor.controller;

import com.example.demo.edoctor.service.EdoctorService;
import org.jcodings.util.Hash;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

@RestController
public class EdoctorController implements ApplicationListener {
    @Autowired
    EdoctorService edoctorService;
    @CrossOrigin
    @PostMapping(value = "/upload")
    public String upload(HttpServletRequest request,@RequestParam(value = "description",required = false,defaultValue = "default") String  description, @RequestParam("file") MultipartFile file){
        String path = request.getServletContext().getRealPath("/upload/")+description+File.separator;
        String filename = file.getOriginalFilename();
        File filePath = new File(path, filename);
        System.out.println("filePath:"+filePath);
        if (!filePath.getParentFile().exists()){
            filePath.getParentFile().mkdirs();
        }
        JSONObject json = new JSONObject();

        try {
            file.transferTo(new File(path+filename));
            System.out.println("path+filename:"+path+filename);
//            HashMap<String,String> hashMap = edoctorService.baiduOcr(path+filename);
            HashMap<String,String> hashMap = edoctorService.ocrService(path+filename,filename);
            json.put("status","success");
            json.put("result",hashMap);
            System.out.println(hashMap.toString());
        } catch (IOException e) {
            e.printStackTrace();
            json.put("status","error");
        }
        return json.toString();
    }
    @CrossOrigin
    @RequestMapping(value = "/home")
    public String home(){
        return "home";
    }
    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        System.out.println("EdoctorController：实例化");
    }
}
