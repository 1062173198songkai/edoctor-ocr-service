package com.example.demo.edoctor.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class Util {
    public static ArrayList<String> JSONObjectToEntity(JSONObject json){
        Integer logId = json.getInt("log_id");
        Integer wordsResultNum = json.getInt("words_result_num");
        JSONArray jsonArray = json.getJSONArray("words_result");
        ArrayList<String> stringList = new ArrayList<String>(jsonArray.length());
        for(int i = 0 ; i < jsonArray.length() ; i ++){
            JSONObject temJsonObject = jsonArray.getJSONObject(i);
            stringList.add(temJsonObject.getString("words"));
        }
        return stringList;

    }
}