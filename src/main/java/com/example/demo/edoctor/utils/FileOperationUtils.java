package com.example.demo.edoctor.utils;

import java.io.File;
import java.io.FileInputStream;

public class FileOperationUtils {
    //读取文件到byte[]
    public static byte[] getFileBytes(String file) {
        try {
            File f = new File(file);
            int length = (int) f.length();
            byte[] data = new byte[length];
            new FileInputStream(f).read(data);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
