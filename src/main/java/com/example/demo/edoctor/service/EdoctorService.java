package com.example.demo.edoctor.service;

import com.baidu.aip.ocr.AipOcr;
import com.example.demo.edoctor.grpc.OcrGrpcClient;
import com.example.demo.edoctor.utils.FileOperationUtils;
import com.example.demo.edoctor.utils.Util;
import com.google.protobuf.ByteString;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.concurrent.*;

@Service
public class EdoctorService implements ApplicationListener {
    //设置APPID/AK/SK
    private static final String APP_ID = "23721806";
    private static final String API_KEY = "hcaUiHZp26mgXlpWIahNFEsG";
    private static final String SECRET_KEY = "jkWTLb9UkkUMKI3EnkW79d9h3jEQ2TPg";
    public static final String[] PaO2Rex = {"氧分压","PO2","PaO2"};
    public static final String[] PaCO2Rex = {"二氧化碳分压","PCO2","PaCO2"};
    public static final String[] phRex = {"pH","PH","ph","酸碱度"};
    public static final String[] ABRex = { "碳酸氢根","HCO3-"};
    public static final String[] SBRex = {"标准碳酸","HCO3-std","SBC"};
    public static final String[] BERex = {"碱剩余","剩余碱","BE"};
    public static final String[] clRex= {"氯","Cl"};
    public static final String[] naRex= {"钠","Na"};
    public static final String[] kRex = {"钾","K"};
    public static final String[] caRex ={"钙","Ca"};
    public static final String[] TAGS = {"PaO2","PaCO2","PH","AB","SB","BE","HCO3-","Cl-","Na+","K+","Ca2+"};
    private static final int OCR_BOARD_LINE = 9;
    private ExecutorService threadPool = Executors.newCachedThreadPool();

    private AipOcr client;

    @Autowired
    private CorrectImageService correctImageService;
    @Autowired
    private OcrGrpcClient ocrGrpcClient;


//    public getOcrResult(){
//
//    }

    public HashMap<String,String> ocrService(String path,String fileName){
        BaiduOcrCallable baiduOcrCallable = new BaiduOcrCallable(path,this);
        Future<HashMap<String,String>> getBaiduOcrResult = threadPool.submit(baiduOcrCallable);
        NativeOcrCallable nativeOcrCallable = new NativeOcrCallable(path,this,fileName);
        Future<HashMap<String,String>> getNativeOcrResult = threadPool.submit(nativeOcrCallable);
        //均没有完成
        while (  !getBaiduOcrResult.isDone() && !getNativeOcrResult.isDone() ){
        }
        boolean successGetBaidu = false;
        boolean successGetNative = false;
        //已经有一个线程获取到了结果
        //baiduOcr 先得到结果
        if(getBaiduOcrResult.isDone()){
            HashMap<String,String> baiduMap = null;
            try {
                baiduMap = getBaiduOcrResult.get();
                if(baiduMap.size() >= 9){
                    return baiduMap;
                }
                successGetBaidu = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            while(!getNativeOcrResult.isDone()){
            }
            HashMap<String,String> nativeMap = null;
            try {
               nativeMap = getNativeOcrResult.get();
                successGetNative = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            if(successGetBaidu && successGetNative){
                //需要合并
                return mergeMap(baiduMap,nativeMap);
            }else {
                if(successGetBaidu){
                    return baiduMap;
                }
                if(successGetNative){
                    return nativeMap;
                }
                return null;
            }
        }else {
            //nativeOCR完成
            HashMap<String,String> nativeMap = null;
            try {
                nativeMap = getNativeOcrResult.get();
                if(nativeMap.size()>=9){
                    return nativeMap;
                }
                successGetNative = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            while(!getBaiduOcrResult.isDone()){
            }
            HashMap<String,String> baiduMap = null;
            //baiduOCr完成
            try {
                baiduMap = getBaiduOcrResult.get();
                if(baiduMap.size()>9){
                    return baiduMap;
                }
                successGetBaidu = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            if(successGetBaidu && successGetNative){
                //需要合并
                return mergeMap(baiduMap,nativeMap);
            }else {
                if(successGetBaidu){
                    return baiduMap;
                }
                if(successGetNative){
                    return nativeMap;
                }
                return null;
            }
        }
    }
    private HashMap<String,String> mergeMap(HashMap<String,String> map1,HashMap<String,String> map2){
        for(String tag : TAGS){
            String tem = map2.get(tag);
            if(tem != null && map1.get(tag) == null){
                map1.put(tag,tem);
            }
        }
        return map1;
    }
    //本地ocr服务
    public HashMap<String,String> nativeOcr(String path,String fileName){
        byte[] bytesImg = FileOperationUtils.getFileBytes(path);
        ByteString bytes = ByteString.copyFrom(bytesImg);
        String result_str = ocrGrpcClient.greet(bytes,fileName);
        StringTokenizer stringTokenizer = new StringTokenizer(result_str,"_");
        ArrayList<String> wordList = new ArrayList<>(stringTokenizer.countTokens());
        while(stringTokenizer.hasMoreTokens()){
            wordList.add(stringTokenizer.nextToken());
        }
        //获取氧分压
        GetInfoCallable getPO2Info = new GetInfoCallable(wordList,PaO2Rex);
        Future<Double> futureP02 = threadPool.submit(getPO2Info);
        //获取二氧化碳分压
        GetInfoCallable getPCO2Info = new GetInfoCallable(wordList,PaCO2Rex);
        Future<Double> futurePC02 = threadPool.submit(getPCO2Info);
        //获取ph
        GetInfoCallable getPHInfo = new GetInfoCallable(wordList,phRex);
        Future<Double> futurePH = threadPool.submit(getPHInfo);
        //获取AB
        GetInfoAgainstCallable getABInfoAgainst = new GetInfoAgainstCallable(wordList,ABRex,SBRex);
        Future<Double> futureAB = threadPool.submit(getABInfoAgainst);
        //获取SB
        GetInfoCallable getSBInfo = new GetInfoCallable(wordList,SBRex);
        Future<Double> futureSB = threadPool.submit(getSBInfo);
        //获取BE
        GetInfoCallable getBEInfo = new GetInfoCallable(wordList,BERex);
        Future<Double> futureBE = threadPool.submit(getBEInfo);
        //获取Cl
        GetInfoCallable getClInfo = new GetInfoCallable(wordList,clRex);
        Future<Double> futureCl = threadPool.submit(getClInfo);
        //获取Na
        GetInfoCallable getNaInfo = new GetInfoCallable(wordList,naRex);
        Future<Double> futureNa = threadPool.submit(getNaInfo);
        //获取K
        GetInfoCallable getKInfo = new GetInfoCallable(wordList,kRex);
        Future<Double> futureK = threadPool.submit(getKInfo);
        //获取Ca
        GetInfoCallable getCaInfo = new GetInfoCallable(wordList,caRex);
        Future<Double> futureCa = threadPool.submit(getCaInfo);
        //最多11项
        HashMap<String,String> resultMap = new HashMap<>(15);
        //各任务均已经完成
        while( !(futureCa.isDone()&&futureK.isDone()&&futureNa.isDone()&&futureCl.isDone()
                &&futureBE.isDone()&&futureSB.isDone()&&futureAB.isDone()&&futurePH.isDone()&&futurePC02.isDone()&&futureP02.isDone()) ){
        }
        //放入氧分压
        try{
            Double tem =futureP02.get();
            if(tem != null){
                resultMap.put("PaO2",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futurePC02.get();
            if(tem != null){
                resultMap.put("PaCO2",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futurePH.get();
            if(tem != null){
                resultMap.put("PH",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureAB.get();
            if(tem != null){
                resultMap.put("AB",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureSB.get();
            if(tem != null){
                resultMap.put("SB",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureBE.get();
            if(tem != null){
                resultMap.put("BE",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureAB.get();
            if(tem != null){
                resultMap.put("HCO3-",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureCl.get();
            if(tem != null){
                resultMap.put("Cl-",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureNa.get();
            if(tem != null){
                resultMap.put("Na+",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureK.get();
            if(tem != null){
                resultMap.put("K+",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureCa.get();
            if(tem != null){
                resultMap.put("Ca2+",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return resultMap;
    }
    private static class NativeOcrCallable implements Callable<HashMap<String,String>>{

        private String path;
        private EdoctorService edoctorService;
        private String fileName;
        public NativeOcrCallable(String path,EdoctorService edoctorService,String fileName){
            this.path = path;
            this.edoctorService = edoctorService;
            this.fileName = fileName;
        }
        @Override
        public HashMap<String, String> call() throws Exception {
            return edoctorService.nativeOcr(path,fileName);
        }
    }
    /**
     * 百度OCR多线程内部静态类
     */
    private static class BaiduOcrCallable implements Callable<HashMap<String,String>>
    {
        private String path;
        private EdoctorService edoctorService;
        public BaiduOcrCallable(String path,EdoctorService edoctorService){
            this.path = path;
            this.edoctorService = edoctorService;
        }
        @Override
        public HashMap<String, String> call() throws Exception {
            return edoctorService.baiduOcr(path);
        }
    }
    /**
     * 当前的图片服务器中临时路径
     * @param path
     * @return
     */
    public HashMap<String,String>baiduOcr(String path){
        String fileExportedImage = null;
//        try {
//            fileExportedImage = correctImageService.correctImage(path);
//            System.out.println("fileExportedImage:"+fileExportedImage);
//        }catch (Throwable throwable){
//            throwable.printStackTrace();
//        }
        // 调用接口
        HashMap<String,String> hashMap = new HashMap<String, String>();
        hashMap.put("detect_direction","true");
//        hashMap.put("probability","true");

        JSONObject res = client.basicAccurateGeneral(path, hashMap);
        System.out.println(res.toString(2));

        ArrayList<String> wordList = Util.JSONObjectToEntity(res);
        //获取氧分压
        GetInfoCallable getPO2Info = new GetInfoCallable(wordList,PaO2Rex);
        Future<Double> futureP02 = threadPool.submit(getPO2Info);
        //获取二氧化碳分压
        GetInfoCallable getPCO2Info = new GetInfoCallable(wordList,PaCO2Rex);
        Future<Double> futurePC02 = threadPool.submit(getPCO2Info);
        //获取ph
        GetInfoCallable getPHInfo = new GetInfoCallable(wordList,phRex);
        Future<Double> futurePH = threadPool.submit(getPHInfo);
        //获取AB
        GetInfoAgainstCallable getABInfoAgainst = new GetInfoAgainstCallable(wordList,ABRex,SBRex);
        Future<Double> futureAB = threadPool.submit(getABInfoAgainst);
        //获取SB
        GetInfoCallable getSBInfo = new GetInfoCallable(wordList,SBRex);
        Future<Double> futureSB = threadPool.submit(getSBInfo);
        //获取BE
        GetInfoCallable getBEInfo = new GetInfoCallable(wordList,BERex);
        Future<Double> futureBE = threadPool.submit(getBEInfo);
        //获取Cl
        GetInfoCallable getClInfo = new GetInfoCallable(wordList,clRex);
        Future<Double> futureCl = threadPool.submit(getClInfo);
        //获取Na
        GetInfoCallable getNaInfo = new GetInfoCallable(wordList,naRex);
        Future<Double> futureNa = threadPool.submit(getNaInfo);
        //获取K
        GetInfoCallable getKInfo = new GetInfoCallable(wordList,kRex);
        Future<Double> futureK = threadPool.submit(getKInfo);
        //获取Ca
        GetInfoCallable getCaInfo = new GetInfoCallable(wordList,caRex);
        Future<Double> futureCa = threadPool.submit(getCaInfo);
        //最多11项
        HashMap<String,String> resultMap = new HashMap<>(15);
        //各任务均已经完成
        while( !(futureCa.isDone()&&futureK.isDone()&&futureNa.isDone()&&futureCl.isDone()
        &&futureBE.isDone()&&futureSB.isDone()&&futureAB.isDone()&&futurePH.isDone()&&futurePC02.isDone()&&futureP02.isDone()) ){
        }
        //放入氧分压
        try{
            Double tem =futureP02.get();
            if(tem != null){
                resultMap.put("PaO2",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futurePC02.get();
            if(tem != null){
                resultMap.put("PaCO2",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futurePH.get();
            if(tem != null){
                resultMap.put("PH",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureAB.get();
            if(tem != null){
                resultMap.put("AB",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureSB.get();
            if(tem != null){
                resultMap.put("SB",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureBE.get();
            if(tem != null){
                resultMap.put("BE",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureAB.get();
            if(tem != null){
                resultMap.put("HCO3-",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureCl.get();
            if(tem != null){
                resultMap.put("Cl-",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureNa.get();
            if(tem != null){
                resultMap.put("Na+",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureK.get();
            if(tem != null){
                resultMap.put("K+",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        try{
            Double tem =futureCa.get();
            if(tem != null){
                resultMap.put("Ca2+",String.valueOf(tem));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return resultMap;
    }
    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
        this.client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(Integer.MAX_VALUE);

        // 可选：设置代理服务器地址, http和socket二选一，或者均不设置
//        client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
//        client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理

        // 可选：设置log4j日志输出格式，若不设置，则使用默认配置
        // 也可以直接通过jvm启动参数设置此环境变量
        System.setProperty("aip.log4j.conf", "path/to/your/log4j.properties");
        System.out.println("EdoctorService：实例化");
    }

    private static class GetInfoCallable implements Callable<Double>{
        public ArrayList<String> words = null;
        public String[] rex = null;
        public GetInfoCallable(ArrayList<String> words, String[] rex){
            this.words = words;
            this.rex = rex;
        }
        private static String findNumber(String string){
            Integer first = null;
            Integer last = null;
            for(int i = 0 ; i < string.length() ; i ++){
                if(first == null){
                    if(string.charAt(i) >='0' && string.charAt(i) <= '9'){
                        first = i;
                    }
                }else {
                    if (string.charAt(i) != '.' && (string.charAt(i) < '0' || string.charAt(i) > '9')) {
                        last = i;
                        break;
                    }
                }
            }
            if(first != null && last != null){
                return string.substring(first,last);
            }
            if(first != null && last == null && string.charAt(string.length()-1) != '.'){
                return string.substring(first);
            }
            return null;
        }
        @Override
        public Double call() throws Exception {
            try{
                Integer index = null;
                String reg = null;
                boolean isFinish = false;
                for(int i = 0 ; i < words.size(); i++){
                    String temWords = words.get(i);
                    for(int j = 0 ; j < rex.length ; j ++){
                        if(temWords.equals(rex[j])){
                            index = i;
                            isFinish = true;
                            reg = rex[j];
                            break;
                        }
                        String[] splitsString = temWords.split(rex[j]);
                        //匹配到了结果
                        if(splitsString.length >= 2 ){
                            index = i;
                            isFinish = true;
                            reg = rex[j];
                            break;
                        }
                    }
                    if(isFinish){
                        break;
                    }
                }
                try {
                    if(index != null && reg != null){
                        String[] splits = words.get(index).split(reg);
                        String tem = splits[1];
                        String numStr = findNumber(tem);
                        Double num = Double.valueOf(numStr);
                        return num;
                    }
                }catch (Throwable throwable){
                    throwable.printStackTrace();
                }
                try {
                    Double num = Double.valueOf(findNumber(words.get(index+1)));
                    return num;
                }catch (Throwable throwable){
                    try {
                        Double num = Double.valueOf(findNumber(words.get(index+2)));
                        return num;
                    }catch (Throwable throwable1){
                        throwable1.printStackTrace();
                        return null;
                    }
                }
            }catch (Throwable throwable){
                throwable.printStackTrace();
                return null;
            }
        }
    }

    private static class GetInfoAgainstCallable implements Callable<Double>{
        public ArrayList<String> words = null;
        public String[] rex = null;
        public String[] rexNotIn = null;

        public GetInfoAgainstCallable(ArrayList<String> words, String[] rex,String[] rexNotIn){
            this.words = words;
            this.rex = rex;
            this.rexNotIn = rexNotIn;
        }
        private static String findNumber(String string){
            Integer first = null;
            Integer last = null;
            for(int i = 0 ; i < string.length() ; i ++){
                if(first == null){
                    if(string.charAt(i) >='0' && string.charAt(i) <= '9'){
                        first = i;
                    }
                }else {
                    if (string.charAt(i) != '.' && (string.charAt(i) < '0' || string.charAt(i) > '9')) {
                        last = i;
                        break;
                    }
                }
            }
            if(first != null && last != null){
                return string.substring(first,last);
            }
            if(first != null && last == null && string.charAt(string.length()-1) != '.'){
                return string.substring(first);
            }
            return null;
        }
        //确保也不在rexNotIn的范围内
        private boolean  isMatchNotIn(String string) {
            boolean isMatch = false;
            for (int i = 0; i < rexNotIn.length; i++) {
                if(rexNotIn[i].equals(string)){
                    isMatch = true;
                    break;
                }
                String[] splitsString = string.split(rexNotIn[i]);
                if(splitsString.length >=2){
                    isMatch = true;
                    break;
                }
            }
            return isMatch;
        }

        @Override
        public Double call() throws Exception {
            try{
                Integer index = null;
                String reg = null;
                for(int i = 0 ; i < words.size(); i++){
                    String temWords = words.get(i);
                    for(int j = 0 ; j < rex.length ; j ++){
                        if(temWords.equals(rex[j])){
                            index = i;
                            reg = rex[j];
                            break;
                        }
                        String[] splitsString = temWords.split(rex[j]);
                        //匹配到了结果
                        if(splitsString.length >= 2 && !isMatchNotIn(temWords)){
                            index = i;
                            reg = rex[j];
                            break;
                        }
                    }
                }
                try {
                    if(index != null && reg != null){
                        String[] splits = words.get(index).split(reg);
                        String tem = splits[1];
                        String numStr = findNumber(tem);
                        Double num = Double.valueOf(numStr);
                        return num;
                    }
                }catch (Throwable throwable){
                    throwable.printStackTrace();
                }
                try {
                    Double num = Double.valueOf(findNumber(words.get(index+1)));
                    return num;
                }catch (Throwable throwable){
                    try {
                        Double num = Double.valueOf(findNumber(words.get(index+2)));
                        return num;
                    }catch (Throwable throwable1){
                        throwable1.printStackTrace();
                        return null;
                    }
                }
            }catch (Throwable throwable){
                throwable.printStackTrace();
                return null;
            }
        }

    }
}
