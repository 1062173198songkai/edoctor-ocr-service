package com.example.demo.edoctor.service;

import com.example.demo.edoctor.utils.HandleImgUtils;
import org.opencv.core.Mat;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.StringTokenizer;

@Service
public class CorrectImageService {
    static {
//        System.load("/Users/admin/Desktop/edoctor/src/main/resources/lib/opencv/libopencv_java401.dylib");
//        OpenCV.loadShared();
    }

    public String correctImage(String imagePath){
        Mat src = HandleImgUtils.matFactory(imagePath);
        StringTokenizer stringTokenizer = new StringTokenizer(imagePath, File.separator);
        StringBuilder imageExportedPath = new StringBuilder();
        int count = stringTokenizer.countTokens();
        int i = 0;
        while(stringTokenizer.hasMoreTokens()){
            imageExportedPath.append( File.separator).append(stringTokenizer.nextToken());
            ++ i;
            if(i == count -2){
                imageExportedPath.append(File.separator).append("export");
            }
        }
        try{
            HandleImgUtils.correct(src,imageExportedPath.toString());
            return imageExportedPath.toString();
        }catch (Throwable throwable){
            return null;
        }
    }
}
