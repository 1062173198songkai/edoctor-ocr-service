package com.example.demo.edoctor.entity;

import java.util.List;

public class Entity {
    private Integer log_id;
    private Integer word_result_num;
    private List<String> word_result;

    public Entity(Integer log_id,Integer word_result_num,List<String> word_result){
        this.log_id = log_id;
        this.word_result_num = word_result_num;
        this.word_result = word_result;
    }

    public int getLog_id() {
        return log_id;
    }

    public int getWord_result_num() {
        return word_result_num;
    }

    public List<String> getWord_result() {
        return word_result;
    }

    public void setWord_result(List<String> word_result) {
        this.word_result = word_result;
    }

    public void setLog_id(int log_id) {
        this.log_id = log_id;
    }


    public void setWord_result_num(int word_result_num) {
        this.word_result_num = word_result_num;
    }
}
