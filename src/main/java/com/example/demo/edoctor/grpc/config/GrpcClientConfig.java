package com.example.demo.edoctor.grpc.config;

import com.example.demo.edoctor.grpc.OcrGrpcClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GrpcClientConfig {
    private static final String HOST="localhost";
    private static final int OCR_PORT = 19021;
    private static final int QUERY_CORRECT_PORT = 19022;

    /**
     * Native OCR
     * @return
     */
    @Bean
    public OcrGrpcClient ocrGrpcClient(){
        OcrGrpcClient ocrGrpcClient = new OcrGrpcClient(HOST, OCR_PORT);
        return ocrGrpcClient;
    }
//
//    /**
//     * QueryCorrect
//     * @return
//     */
//    @Bean
//    public QueryCorrectGrpcClient queryCorrectGrpcClient(){
//        QueryCorrectGrpcClient queryCorrectGrpcClient = new QueryCorrectGrpcClient(HOST,QUERY_CORRECT_PORT);
//        return queryCorrectGrpcClient;
//    }

}
