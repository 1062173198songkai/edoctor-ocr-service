package com.example.demo.edoctor.grpc;

import com.example.demo.edoctor.utils.FileOperationUtils;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;

public class OcrGrpcClient {
    private final ManagedChannel channel;
//    private final GreeterGrpc.GreeterBlockingStub blockingStub;
    private final GreeterGrpc.GreeterBlockingStub blockingStub;
    private final GreeterGrpc.GreeterStub greeterStub;
    public OcrGrpcClient (String host,int port){
        this(ManagedChannelBuilder.forAddress(host,port).usePlaintext().build());
    }
    OcrGrpcClient(ManagedChannel channel){
        this.channel = channel;
        this.blockingStub = GreeterGrpc.newBlockingStub(channel);
        this.greeterStub = GreeterGrpc.newStub(channel);
    }
    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }
    /**
     * send to server.
     */
    public String greet(ByteString byteString,String picname) {
        OcrRequest request = OcrRequest.newBuilder().setPics(byteString).setPicname(picname).build();
        OcrReply ocrReply;
        try {
            ocrReply = blockingStub.sendOcr(request);
            return ocrReply.getResult();
        } catch (StatusRuntimeException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static void main(String[] arg0) throws UnsupportedEncodingException {
//        ByteString bytes = ByteString.copyFrom("123","UTF-8");
//        File file = new File("/Users/admin/Desktop/IMG_6040\\(20210305-212841\\).JPG");
        byte[] bytesImg = FileOperationUtils.getFileBytes("/Users/admin/Desktop/IMG_6042.PNG");
        ByteString bytes = ByteString.copyFrom(bytesImg);
        String str = new OcrGrpcClient("localhost",19021).greet(bytes,"6.PNG");
        System.out.println(str);
    }
}
