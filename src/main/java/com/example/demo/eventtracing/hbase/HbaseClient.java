package com.example.demo.eventtracing.hbase;

import com.example.demo.eventtracing.entity.HotEntity;
import com.example.demo.eventtracing.utils.TimeUtils;
import com.example.demo.search.entity.HotWord;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.client.coprocessor.LongColumnInterpreter;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;


@Service
public class HbaseClient  implements ApplicationRunner {
    private static final String SEARCH_HISTORY_TABLE_NAME = "search:search_history";
    private static final String ELEMENT_SHOW_TABLE_NAME = "search:element_show";
    private static final String ELEMENT_NAME_FILED = "element";
    private static final String ELEMENT_PARAM_FILED = "param";
    private static final String SEARCH_HISTORY_FILED = "query";
    private static final String ALI_HBASE_OUTWEB = "hb-proxy-pub-8vbxo3i6m3sc4utlq-001.hbase.zhangbei.rds.aliyuncs.com";
    private static final String BAIDU_ECS_OUTWEB = "localhost";
    private static final int YESTERDAY = -100;
//    private static final int PAGE_NUMBER = 100;
    private static final int DEFAULT_HOT_WORDS_NUM = 10;
    public static final String FORMART_INT_TO_10NUM = "%010d";
    static Configuration config = null;
    private Connection connection = null;
    private TableName tbname = null;
    private TableName elementShowTableName = null;
    //通过connection获取相应的表
    private Table table = null;
    private Table elementShowTable = null;
    private static AtomicInteger atomicInteger = new AtomicInteger(0);
    private static List<HotEntity> hotWords = null;

    public List<HotEntity> getHotWords(){
        return hotWords;
    }
    public HbaseClient() {
        try {
            config = HBaseConfiguration.create();// 配置
            config.set("hbase.zookeeper.quorum", ALI_HBASE_OUTWEB);// zookeeper地址
//            config.set("hbase.zookeeper.quorum", BAIDU_ECS_OUTWEB);// zookeeper地址
            config.set("hbase.zookeeper.property.clientPort", "2181");// zookeeper端口
//            config.set("hbase.master", "106.13.104.168:60000");
            connection = ConnectionFactory.createConnection(config);
            tbname = TableName.valueOf(SEARCH_HISTORY_TABLE_NAME);
            elementShowTableName = TableName.valueOf(ELEMENT_SHOW_TABLE_NAME);
            table = connection.getTable(tbname);
            elementShowTable = connection.getTable(elementShowTableName);
            System.out.println("hbase实例化");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
    private String rowKeyTactics() {
        int num = atomicInteger.getAndIncrement();
        return TimeUtils.getCurrentDay() + String.format(FORMART_INT_TO_10NUM, num);
    }
    public boolean insertElementShow(String param,String elemtnName,String uuid){
        System.out.println("元素曝光埋点插入");
        //hbase支持批量写入数据，创建Put集合来存放批量的数据
        List<Put> putList = new ArrayList<>(1);
        //主键为当天时间 yymmdd
        Put put = new Put(Bytes.toBytes(uuid));
        put.addColumn(Bytes.toBytes(ELEMENT_NAME_FILED), Bytes.toBytes("name"), Bytes.toBytes(elemtnName));
        put.addColumn(Bytes.toBytes(ELEMENT_PARAM_FILED), Bytes.toBytes("param"), Bytes.toBytes(param));
        putList.add(put);
        try {
            table.put(putList);
            System.out.println("元素曝光埋点插入成功");
            return true;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return false;
        }
    }
    /**
     * 向hbase中插入搜索历史
     * 即加入表中的前三条数据
     *
     * @throws Exception
     */
    public boolean insertHistoryData(String query) {
        System.out.println("搜索历史埋点插入开始");
        //hbase支持批量写入数据，创建Put集合来存放批量的数据
        List<Put> putList = new ArrayList<>(1);
        //主键为当天时间 yymmdd
        Put put = new Put(Bytes.toBytes(rowKeyTactics()));
        put.addColumn(Bytes.toBytes(SEARCH_HISTORY_FILED), Bytes.toBytes("query"), Bytes.toBytes(query));
        putList.add(put);
        try {
            table.put(putList);
            System.out.println("搜索历史埋点插入成功");
            return true;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return false;
        }
    }

    @Scheduled(cron = "0 0 8 * * ? ")
    public void GetHotWordFromHBase() {
        //rowkey大于1009的数据
        String day = TimeUtils.getDaysTimestamp(YESTERDAY);
        Scan scan=new Scan();
        RowFilter filter = new RowFilter(CompareFilter.CompareOp.GREATER, new BinaryComparator((day + String.format(FORMART_INT_TO_10NUM, 0)).getBytes()));
        List<Filter> filters = new ArrayList <Filter>(1);
        filters.add(filter);
        FilterList filterList = new FilterList (FilterList.Operator.MUST_PASS_ALL,filters);
        scan.setFilter(filterList);
        ResultScanner scanner;
        HashMap<String,Integer> hashMap = new HashMap<String, Integer>();
        try {
            scanner = table.getScanner(scan);
            for(Result result : scanner){
                System.out.println(result.toString());
                List<Cell> listCell = result.listCells();
                //默认search_history仅有一个表
                Cell cell = listCell.get(0);
                String rowkey =  Bytes.toString(CellUtil.cloneRow(cell));
                long timestamp=cell.getTimestamp();//取时间戳
                String value=Bytes.toString(CellUtil.cloneValue(cell)); //取值
                Integer integer = hashMap.get(value);
                if(integer == null){
                    integer = 0;
                }
                ++ integer;
                hashMap.put(value,integer);
            }
            Map<String,Integer> resultMap = sortMapByValue(hashMap);
            List<HotEntity> wordList = new ArrayList<HotEntity>(DEFAULT_HOT_WORDS_NUM);
            int i = 1;
            for(Map.Entry<String,Integer> entry:resultMap.entrySet()){
                if(i >= DEFAULT_HOT_WORDS_NUM){
                    break;
                }
                wordList.add(new HotEntity(entry.getKey(),entry.getValue()));
                System.out.println(entry.getKey()+":"+entry.getValue());
                ++ i;
            }
            HbaseClient.hotWords = wordList;
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            atomicInteger.set(0);
        }


    }
    /**
     * 使用 Map按value进行排序
     * @param oriMap
     * @return
     */
    private static Map<String, Integer> sortMapByValue(Map<String, Integer> oriMap) {
        if (oriMap == null || oriMap.isEmpty()) {
            return null;
        }
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        List<Map.Entry<String, Integer>> entryList = new ArrayList<Map.Entry<String, Integer>>(
                oriMap.entrySet());
        Collections.sort(entryList, new MapValueComparator());

        Iterator<Map.Entry<String, Integer>> iter = entryList.iterator();
        Map.Entry<String, Integer> tmpEntry = null;
        while (iter.hasNext()) {
            tmpEntry = iter.next();
            sortedMap.put(tmpEntry.getKey(), tmpEntry.getValue());
        }
        return sortedMap;
    }
    private static class MapValueComparator implements Comparator<Map.Entry<String, Integer>> {

        @Override
        public int compare(Map.Entry<String, Integer> me1, Map.Entry<String, Integer> me2) {

            return me2.getValue().compareTo(me1.getValue());
        }

    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        GetHotWordFromHBase();
    }
}
