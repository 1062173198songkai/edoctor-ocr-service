package com.example.demo.eventtracing.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class HotEntity {
    private String hotword;
    private int num;

}
