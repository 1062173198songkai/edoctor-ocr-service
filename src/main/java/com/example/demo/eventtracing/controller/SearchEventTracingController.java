package com.example.demo.eventtracing.controller;

import com.example.demo.eventtracing.hbase.HbaseClient;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchEventTracingController {
    @Autowired
    HbaseClient hbaseClient;
    //元素曝光埋点
    @PostMapping("/event/element")
    public void postElementShow(@RequestParam("param") String param,@RequestParam("element") String elementName,@RequestParam("uuid")String uuid){
        hbaseClient.insertElementShow(param,elementName,uuid);
    }
}
