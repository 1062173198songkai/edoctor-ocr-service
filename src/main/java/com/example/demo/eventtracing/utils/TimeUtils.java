package com.example.demo.eventtracing.utils;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {
    public static final String DATE_FORMAT = "yyyyMMdd";
    /**
     * 传入一个时间戳
     * 返回该时间戳所在天数信息
     * @param timestamp
     * @return
     */
    public static String gettDayFromTimestamp(Long timestamp,String pattern){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        return simpleDateFormat.format(new Date(timestamp));
    }

    /**
     * 获取当前日期字符串
     * 格式"yymmdd"
     * @return
     */
    public static String getCurrentDay(){

        return gettDayFromTimestamp(System.currentTimeMillis(),DATE_FORMAT);
    }
    /**
     * 获取n天前日期字符串
     * 格式"yymmdd"
     * @return
     */
    public static String getDaysTimestamp(int days){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE,days);
        return gettDayFromTimestamp(calendar.getTimeInMillis(),DATE_FORMAT);
    }
}
